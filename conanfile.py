from conans import ConanFile, tools
import shutil

class RapidcsvConan(ConanFile):
    name           = "rapidcsv"
    license        = "BSD-3-Clause"
    author         = "toge.mail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-rapidcsv/"
    homepage       = "https://github.com/d99kris/rapidcsv/"
    description    = "Rapidcsv is a C++ header-only library for CSV parsing."
    topics         = ("csv", "parser", "header only")
    no_copy_source = True

    def source(self):
        # tools.get("https://github.com/d99kris/rapidcsv/archive/v{}.zip".format(self.version))
        tools.get(**self.conan_data["sources"][self.version])
        shutil.move("rapidcsv-{}".format(self.version), "rapidcsv")

    def package(self):
        self.copy("*.h", dst="include", src="rapidcsv/src")

    def package_id(self):
        self.info.header_only()
